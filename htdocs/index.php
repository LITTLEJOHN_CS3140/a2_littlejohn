
<!DOCTYPE html>
<html lang= "en">
    <head>
        <meta charset = "UTF-8">
		<link rel="stylesheet" href= "styles.css"/>
        <title>Art Store</title>
    </head>
    <body>
        <header class = "black-top">
		<p><a class="nav-link" href = ""> My Account</a> 
		   <a class="nav-link" href = ""> Wishlist</a> 
		   <a class="nav-link" href = ""> Shopping Cart</a> 
		   <a class="nav-link" href = ""> Checkout</a></p>
        </header>
		<div>
		    <h1>Art Store</h1>
		</div>
        <div class = "grey-home">
		<p><a class="nav-link" href = ""> Home</a> 
		   <a class="nav-link"href="page2.php">About Us</a> 
		   <a class="nav-link" href = ""> Art Works</a> 
		   <a class="nav-link" href = ""> Artists</a></p>
		</div>
		
		<div class = "theBigOne">
		    <h2>Self-Portriat in a Straw Hat</h2>
			<p>
			    by <a href="https://en.wikipedia.org/wiki/%C3%89lisabeth_Vig%C3%A9e_Le_Brun">Louise Elisabeth Lebrun</a>
		    </p>
			
			<figure>
			    <img class ="bigImage"src="images/113010.jpg" alt="Louise Lebrun">
				<figcaption>Lebrun's original is recorded in a provate collection in france.</figcaption>
			</figure>
		    <p class= "biography">
			   The painting appears, after cleaning, to be an autograph replica of a picture, the original
			   of which was painted in Brussels in 1782 in free imitation of Ruben's Chapeau de Palle' which
			   LeBrun had seen in Antwerp. It was exhibited in Paris in 1782 at the Salon de la Correspondance.
			</p>
			<p class= "dollar">$700</p>
			
			<div class= "buttons">
			    <a href= ""><button class="button">Add to Wishlist</button></a>
			    <a href= ""><button class="button">Add to Shopping Cart</button></a>
			</div>
			<p class= "product-text">
			    Product Details
			</p>
            <div class="product">
                <table>
                    <tr>
                        <td><strong>Date:</strong></td>
                        <td>1782</td>
                    </tr>
                    <tr>
                        <td><strong>Medium:</strong> </td>
                        <td>Oil on canvas</td>
                    </tr>
                    <tr>
                        <td><strong>Dimensions:</strong></td>
                        <td>98cm x 71c</td>
                    </tr>
                    <tr>
                        <td><strong>Home:</strong> </td>
                        <td><a href="https://www.nationalgallery.org.uk">National Gallery, London</a></td>
                    </tr>
                    <tr>
                        <td><strong>Genres:</strong></td>
                        <td><a href="https://en.wikipedia.org/wiki/Realism_(arts)">Realism
                        </a>, <a href="https://en.wikipedia.org/wiki/Rococo">Rococo</a></td>
                    </tr>
                    <tr>
                        <td><strong>Subjects:</strong>  </td>
                        <td><a href="https://www.wikiart.org/en/louise-elisabeth-vigee-le-brun">People</a>, 
                            <a href="https://en.wikipedia.org/wiki/Art">Arts</a></td>
                    </tr>
                </table>
            </div>
            <div class="similar">
                <p class = "product-text"> Similar Products</p>
				
                <div class="inline-block">
                    <a href="https://en.wikipedia.org/wiki/Portrait_of_the_Artist_Holding_a_Thistle"><figure>
                        <img src="images/thumbs/116010.jpg" alt="Artist Holding a Thistle" class="sim-images"/>
                    <figcaption>Artist Holding a Thistle</figcaption>
                    </figure></a>
                </div>
                <div class="inline-block">
                    <a href="https://en.wikipedia.org/wiki/Portrait_of_Eleanor_of_Toledo"><figure>
                        <img src="images/thumbs/120010.jpg" alt="Portrait of Eleanor of Toledo" class="sim-images"/>
                        <figcaption> Portrait of Eleanor of Toledo </figcaption>
                    </figure></a>
                </div>
                <div class="inline-block">
                    <a href="https://en.wikipedia.org/wiki/Madame_de_Pompadour"><figure>
                        <img src="images/thumbs/107010.jpg" alt="Madame de Pompadour" class="sim-images"/>
                        <figcaption> Madame de Pompadour </figcaption>
                    </figure></a>
                </div>
                <div class="inline-block">
                    <a href="https://en.wikipedia.org/wiki/Girl_with_a_Pearl_Earring"> <figure>
                        <img src="images/thumbs/106020.jpg" alt="Girl with a Pearl Earring" class="sim-images"/>
                        <figcaption> Girl with a Pearl Earring </figcaption>
                    </figure> </a>
                </div>
            </div>
		</div>
		<div>
		    <footer class="black-bottom">
		        <p> All images are copyright to their owners. This is just a hypothetical site &copy 2014 Copyright Art Store</p>
		    </footer>
		</div>
		
	</body>
</html>

