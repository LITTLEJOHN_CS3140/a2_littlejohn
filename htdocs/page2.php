<html>
    <head>
        <title>Art Store</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="styles.css"/>
    </head>
    <body>
        <header class="black-top">
            <p><a class="nav-link"href="">My Account</a> <a class="nav-link"href="">Wishlist</a> 
            <a class="nav-link"href=""> Shopping Cart</a> <a class="nav-link"href="">Checkout</a></p>
        </header>
        <h1 class="heading">Art Store</h1>
        <div class = "grey-home">
		<p><a class="nav-link" href = ""> Home</a> 
		   <a class="nav-link"href="page2.php">About Us</a> 
		   <a class="nav-link" href = ""> Art Works</a> 
		   <a class="nav-link" href = ""> Artists</a></p>
		</div>
        <div class="theBigOne">
            <p style="padding: 0 0 0 1%;"><font size="6"><strong>About Us</strong></font></p>
            <p style="padding: 0 0 0 1%;"> 
                This assignment was created by Daniel Littlejohn/ Joshua Opria.
                <br>
                <br>
                It was created for CS3140 at Bowling Green University.
            </p>
        </div>
        <footer class="black-bottom">
		    <p> All images are copyright to their owners. This is just a hypothetical site &copy 2014 Copyright Art Store</p>
		</footer>
    </body>
</html>
